# Datetime Timezone Module

[Datetime Timezone](https://www.drupal.org/project/datetime_timezone) Provides a new field type and widget to allow a
user defined timezone when entering dates.

## Requirements

* Drupal 9

## Installation

Datetime Timezone can be installed via the
[standard Drupal installation process](http://drupal.org/node/895232).

## Usage

* For example, create a new field with type *datetime_timezone*.
* Select the timezone widget for the form field.
* Select the timezone formatter for the rendered field.
* Create a new content with the field.
