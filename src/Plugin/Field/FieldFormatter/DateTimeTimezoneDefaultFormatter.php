<?php

declare(strict_types=1);

namespace Drupal\datetime_timezone\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'Default' formatter for 'datetime_timezone'.
 *
 * @FieldFormatter(
 *   id = "datetime_timezone_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "datetime_timezone"
 *   }
 * )
 */
class DateTimeTimezoneDefaultFormatter extends DateTimeTimezoneFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $build = [];

    foreach ($items as $delta => $item) {
      if ($item->value) {
        // Create the ISO date in Universal Time.
        $date = $item->date;
        $iso_date = $date->format(\DateTimeInterface::ATOM, ['timezone' => 'UTC']);
        $build[$delta] = [
          '#theme' => 'time',
          '#text' => $this->formatDate($date, $item->timezone),
          '#attributes' => [
            'datetime' => $iso_date,
          ],
          '#cache' => [
            'contexts' => [
              'timezone',
            ],
          ],
        ];
      }
    }

    return $build;
  }

}
