<?php

declare(strict_types=1);

namespace Drupal\datetime_timezone\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Parent plugin for datetime timezone formatters.
 */
abstract class DateTimeTimezoneFormatterBase extends FormatterBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The date format entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $dateFormatStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    $instance = new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings']
    );

    $instance->dateFormatter = $container->get('date.formatter');
    $instance->dateFormatStorage = $container->get('entity_type.manager')->getStorage('date_format');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'format_type' => 'medium',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['format_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Date format'),
      '#description' => $this->t('Choose a format for displaying the date. Be sure to set a format appropriate for the field, i.e. omitting time for a field that only has a date.'),
      '#options' => $this->getDateFormatOptions(),
      '#default_value' => $this->getSetting('format_type'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Format: @display', ['@display' => $this->formatDate(new DrupalDateTime())]);
    return $summary;
  }

  /**
   * Formats the date with the selected type.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   The date object.
   * @param string|null $timezone
   *   The timezone if we want to take it into account when rendering.
   *
   * @return string
   *   The formatted date.
   */
  protected function formatDate(DrupalDateTime $date, ?string $timezone = NULL): string {
    $format_type = $this->getSetting('format_type');
    return $this->dateFormatter->format($date->getTimestamp(), $format_type, '', $timezone);
  }

  /**
   * Gets the date format options.
   *
   * @return array
   *   An array of options to be used in the form.
   *
   * @see \Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeDefaultFormatter
   */
  protected function getDateFormatOptions() {
    $time = new DrupalDateTime();
    $format_types = $this->dateFormatStorage->loadMultiple();
    $options = [];
    foreach ($format_types as $type => $type_info) {
      $format = $this->dateFormatter->format($time->getTimestamp(), $type);
      $options[$type] = sprintf('%s (%s)', $type_info->label(), $format);
    }

    return $options;
  }

}
