<?php

declare(strict_types=1);

namespace Drupal\datetime_timezone\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Plugin implementation of the 'datetime_timezone' field type.
 *
 * @FieldType(
 *   id = "datetime_timezone",
 *   label = @Translation("Date with timezone"),
 *   description = @Translation("Create and store date with timezone information."),
 *   default_widget = "datetime_timezone",
 *   default_formatter = "datetime_timezone_default",
 *   list_class = "\Drupal\datetime\Plugin\Field\FieldType\DateTimeFieldItemList"
 * )
 */
class DateTimeTimezoneItem extends DateTimeItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['timezone'] = DataDefinition::create('string')
      ->setLabel(t('Timezone'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['timezone'] = [
      'type' => 'varchar',
      'length' => 255,
      'description' => 'Timezone',
    ];

    return $schema;
  }

}
