<?php

declare(strict_types=1);

namespace Drupal\datetime_timezone\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\TimeZoneFormHelper;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\datetime\Plugin\Field\FieldWidget\DateTimeDefaultWidget;

/**
 * Plugin implementation of the 'datetime_timezone' widget.
 *
 * @FieldWidget(
 *   id = "datetime_timezone",
 *   label = @Translation("Date and time with timezone"),
 *   field_types = {
 *     "datetime",
 *     "datetime_timezone"
 *   }
 * )
 */
class DateTimeTimezoneWidget extends DateTimeDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    if ($items[$delta]->timezone) {
      // Make sure we convert back to the user selected timezone from the
      // storage timezone.
      $timezone = new \DateTimeZone($items[$delta]->timezone);
      $element['value']['#default_value']->setTimeZone($timezone);
      $element['value']['#date_timezone'] = $items[$delta]->timezone;
    }

    $element['timezone'] = [
      '#type' => 'select',
      '#title' => $this->t('Timezone'),
      '#options' => TimeZoneFormHelper::getOptionsListByRegion(),
      '#description' => $this->t('Select the timezone in which the date should be stored and displayed.'),
      '#default_value' => $items[$delta]->timezone,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $value) {
      if (empty($value['timezone'])) {
        continue;
      }

      $timezone = new \DateTimezone($value['timezone']);

      // We now override the value with the entered value converted into the
      // selected timezone, and then DateTimeWidgetBase converts this value
      // into UTC for storage.
      if ($value['value'] instanceof DrupalDateTime) {
        $values[$delta]['value'] = new DrupalDateTime($value['value']->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT), $timezone);
      }
    }

    return parent::massageFormValues($values, $form, $form_state);
  }

}
