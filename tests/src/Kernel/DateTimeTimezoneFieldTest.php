<?php

declare(strict_types=1);

namespace Drupal\Tests\datetime_timezone\Kernel;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;

/**
 * Tests the datetime_timezone field type definition.
 *
 * @group datetime_timezone
 */
class DateTimeTimezoneFieldTest extends EntityKernelTestBase {

  /**
   * A field storage to use in this test class.
   *
   * @var \Drupal\field\Entity\FieldStorageConfig
   */
  protected $fieldStorage;

  /**
   * The field used in this test class.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $field;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'node',
    'datetime',
    'datetime_timezone',
    'system',
    'text',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('user', 'users_data');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installConfig(['field', 'node', 'system']);

    // Create content type.
    $type = NodeType::create([
      'name' => 'Test content type',
      'type' => 'test_ct',
    ]);
    $type->save();

    $this->fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_datetime',
      'entity_type' => 'node',
      'type' => 'datetime_timezone',
      'cardinality' => 1,
      'entity_types' => ['node'],
    ]);
    $this->fieldStorage->save();

    $this->field = FieldConfig::create([
      'label' => 'Datetime timezone field',
      'field_name' => 'field_datetime',
      'entity_type' => 'node',
      'bundle' => 'test_ct',
      'settings' => [],
      'required' => FALSE,
    ]);
    $this->field->save();
  }

  /**
   * Test the datetime timezone field.
   */
  public function testDatetimeTimezoneField(): void {
    $request_time = $this->getNow()->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $values = [
      'type' => 'test_ct',
      'title' => 'My node title',
      'field_datetime' => [
        'value' => $request_time,
        'timezone' => 'Europe/Budapest',
      ],
    ];

    $node = Node::create($values);
    $node->save();

    $node_storage = $this->container->get('entity_type.manager')->getStorage('node');
    $node_storage->resetCache();
    /** @var \Drupal\node\NodeInterface $node */
    $node = $node_storage->load($node->id());

    // Assert field values.
    $expected = [
      [
        'value' => $request_time,
        'timezone' => 'Europe/Budapest',
      ],
    ];
    $this->assertEquals('My node title', $node->label());
    $this->assertEquals($expected, $node->get('field_datetime')->getValue());

    // Assert empty field.
    $values = [
      'type' => 'test_ct',
      'title' => 'My second node',
      'field_datetime' => [
        [
          'value' => '',
          'timezone' => '',
        ],
      ],
    ];

    $node = Node::create($values);
    $node->save();

    // Assert the base field values.
    $this->assertEquals('My second node', $node->label());
    $this->assertTrue($node->get('field_datetime')->isEmpty());
  }

  /**
   * Gets current time.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   Current time.
   */
  protected function getNow(): DrupalDateTime {
    $request_time = $this->container->get('datetime.time')->getRequestTime();
    return DrupalDateTime::createFromTimestamp($request_time);
  }

}
