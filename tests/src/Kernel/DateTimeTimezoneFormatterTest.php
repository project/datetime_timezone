<?php

declare(strict_types=1);

namespace Drupal\Tests\datetime_timezone\Kernel;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test the default formatter.
 *
 * @group datetime_timezone
 */
class DateTimeTimezoneFormatterTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'user',
    'system',
    'field',
    'text',
    'entity_test',
    'datetime',
    'datetime_timezone',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['system']);
    $this->installEntitySchema('entity_test');

    // Make sure medium date format has 'D, j M Y - H:i' pattern.
    DateFormat::load('medium')
      ->setPattern('D, j M Y - H:i')
      ->save();

    EntityViewDisplay::create([
      'targetEntityType' => 'entity_test',
      'bundle' => 'entity_test',
      'mode' => 'default',
    ])->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'date',
      'entity_type' => 'entity_test',
      'type' => 'datetime_timezone',
    ]);
    $field_storage->save();

    $field_config = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'entity_test',
    ]);
    $field_config->save();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository */
    $entity_display_repository = \Drupal::service('entity_display.repository');

    $entity_display_repository->getViewDisplay('entity_test', 'entity_test', 'default')
      ->setComponent('date', [
        'type' => 'datetime_timezone_default',
      ])
      ->save();
  }

  /**
   * Ensure the field is rendered in the correct timezone.
   */
  public function testRenderedInCorrectTimezone() {
    $timezone = 'America/New_York';
    $date = new DrupalDateTime('2017-03-25 10:30:00', 'UTC');
    $entity = EntityTest::create([
      'date' => [
        'value' => $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
        'timezone' => $timezone,
      ],
    ]);
    $entity->save();

    // Render the field.
    $output = $entity->date->view([]);

    // Ensure the date is formatted using America/New York timezone using
    // the medium date format type.
    $this->assertEquals('Sat, 25 Mar 2017 - 06:30', $output[0]['#text']);
    $this->assertEquals('2017-03-25T10:30:00+00:00', $output[0]['#attributes']['datetime']);
  }

}
