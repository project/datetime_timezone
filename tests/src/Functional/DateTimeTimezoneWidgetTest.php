<?php

declare(strict_types=1);

namespace Drupal\Tests\datetime_timezone\Functional;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the datetime timezone widget.
 *
 * @group datetime_timezone
 */
class DateTimeTimezoneWidgetTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['entity_test', 'datetime_timezone'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_datetime',
      'entity_type' => 'entity_test',
      'type' => 'datetime_timezone',
    ]);
    $field_storage->save();

    $field_config = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'entity_test',
    ]);
    $field_config->save();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository */
    $entity_display_repository = \Drupal::service('entity_display.repository');

    $entity_display_repository->getFormDisplay('entity_test', 'entity_test', 'default')
      ->setComponent('field_datetime', [
        'type' => 'datetime_timezone',
      ])
      ->save();

    $this->drupalLogin($this->drupalCreateUser([
      'view test entity',
      'administer entity_test content',
    ]));
  }

  /**
   * Ensure that default values are converted into the selected timezone.
   */
  public function testWidget() {
    // Create a test node that we will edit to test the widget.
    $values = [
      'type' => 'entity_test',
      'title' => 'My node title',
    ];
    $entity = EntityTest::create($values);
    $entity->save();

    $this->drupalGet($entity->toUrl('edit-form'));

    // Fill in the field with a date that we enter in America/New_York timezone.
    $date = new DrupalDateTime('2022-01-25 10:30:00', 'America/New_York');
    $date_format = DateFormat::load('html_date')->getPattern();
    $time_format = DateFormat::load('html_time')->getPattern();
    $edit = [
      'field_datetime[0][value][date]' => $date->format($date_format),
      'field_datetime[0][value][time]' => $date->format($time_format),
      'field_datetime[0][timezone]' => 'America/New_York',
    ];
    $this->submitForm($edit, 'Save');

    // Assert the field values after save.
    $this->assertSession()->fieldValueEquals('field_datetime[0][value][date]', $date->format($date_format));
    $this->assertSession()->fieldValueEquals('field_datetime[0][value][time]', $date->format($time_format));
    $this->assertEquals('America/New_York', $this->assertSession()->selectExists('Timezone')->getValue());

    // Assert the stored values reloading the entity.
    $entity = EntityTest::load($entity->id());
    // Convert the date back to storage timezone to assert the form value had
    // successfully massaged back to storage timezone.
    $date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $this->assertEquals($date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT), $entity->get('field_datetime')->value);
  }

}
